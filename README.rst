======================================
 Team Fortress Classic Config Scripts
======================================

Just copy to the proper directory.

Maybe even run::

  git clone git@gitlab.com:teerl/tfc-cfg.git ./

right in the ``tfc/`` directory if you want.

:resetbinds.cfg:
   Sets all binds to the defaults
:userconfig.cfg:
   Nonstandard config stuff.
